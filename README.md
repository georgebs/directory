# PublicCode Directory & Map

Free and Open Source Software is everywhere in the public sector. This project seeks to create a large directory of software to map where it is being reused – following the principle of [Public Money, Public Code](https://publiccode.eu). All information will be displayed on [publiccode.directory](https://publiccode.directory).

## The database

The Free Software Foundation Europe, in cooperation with OpenUK, is collecting entries in a decentralised manner. This repository is where the FSFE stores and maintains their data, but the [database file](https://github.com/OpenUK/publiccode.directory/blob/master/database/database.index.json) will eventually include entries by other organisations as well.

## The format

In order to guarantee ideal inclusion in the directory and map, all JSON entries have to follow a [common format](https://github.com/OpenUK/publiccode.directory/blob/master/entry-files/entry.json). All fields are also [well explained](https://github.com/OpenUK/publiccode.directory/blob/master/entry-files/Readme.md).

## Contribute

If you would like to have your software added or would like to help extend information, please [get in touch with us](https://fsfe.org/contact). Even for non-technical users there are excellent ways to contribute.
